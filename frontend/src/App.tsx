import { BrowserRouter, Routes, Route } from "react-router-dom";
import HomePage from "./HomePage";

export const App = () => (
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<HomePage />} />
    </Routes>
  </BrowserRouter>
);
