import { Container } from "@chakra-ui/react";
import { AddContact, ListContact } from "./components/contact";
import Hero from "./components/hero";

const HomePage = () => {
  return (
    <>
      <Hero />

      <Container maxW={"7xl"} p="12">
        <AddContact />
        <hr />
        <ListContact />
      </Container>
    </>
  );
};

export default HomePage;
