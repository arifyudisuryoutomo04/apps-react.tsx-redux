import axios from "axios";

export const GET_LIST_CONTACT = "GET_LIST_CONTACT";
export const ADD_CONTACT = "ADD_CONTACT";
export const DELETE_CONTACT = "DELETE_CONTACT";
export const DETAIL_CONTACT = "DETAIL_CONTACT";
export const UPDATE_CONTACT = "UPDATE_CONTACT";

export const getListContact = () => {
  //? test proccess
  // console.log("2. enter action");
  //? end test proccess

  return (dispatch: any) => {
    // loading
    dispatch({
      type: GET_LIST_CONTACT,
      payload: {
        loading: true,
        data: false,
        errorMessage: false
      }
    });

    //? Success Get API
    axios({
      method: "GET",
      url: "http://localhost:3000/contact",
      timeout: 12000
    })
      .then((response) => {
        //? test proccess
        // console.log("3. success get data", response.data);
        //? end test proccess

        dispatch({
          type: GET_LIST_CONTACT,
          payload: {
            loading: false,
            data: response.data,
            errorMessage: false
          }
        });
      })
      //? failed Get API
      .catch((error) => {
        //? test proccess
        // console.log("3. failed get data", error);
        //? end test proccess

        dispatch({
          type: GET_LIST_CONTACT,
          payload: {
            loading: false,
            data: false,
            errorMessage: error.message
          }
        });
      });
  };
};

export const addContact = (data: any) => {
  //? test proccess
  // console.log("2. enter action");
  //? end test proccess

  return (dispatch: any) => {
    // loading
    dispatch({
      type: ADD_CONTACT,
      payload: {
        loading: true,
        data: false,
        errorMessage: false
      }
    });

    //? Success Get API
    axios({
      method: "POST",
      url: "http://localhost:3000/contact",
      timeout: 12000,
      data: data
    })
      .then((response) => {
        //? test proccess
        // console.log("3. success get data", response.data);
        //? end test proccess

        dispatch({
          type: ADD_CONTACT,
          payload: {
            loading: false,
            data: response.data,
            errorMessage: false
          }
        });
      })
      //? failed Get API
      .catch((error) => {
        //? test proccess
        // console.log("3. failed get data", error);
        //? end test proccess

        dispatch({
          type: ADD_CONTACT,
          payload: {
            loading: false,
            data: false,
            errorMessage: error.message
          }
        });
      });
  };
};

export const deleteContact = (id: any) => {
  //? test proccess
  // console.log("2. enter action");
  //? end test proccess

  return (dispatch: any) => {
    // loading
    dispatch({
      type: DELETE_CONTACT,
      payload: {
        loading: true,
        data: false,
        errorMessage: false
      }
    });

    //? Success Get API
    axios({
      method: "DELETE",
      url: `http://localhost:3000/contact/` + id,
      timeout: 12000
    })
      .then((response) => {
        //? test proccess
        // console.log("3. success get data", response.data);
        //? end test proccess

        dispatch({
          type: DELETE_CONTACT,
          payload: {
            loading: false,
            data: response.data,
            errorMessage: false
          }
        });
      })
      //? failed Get API
      .catch((error) => {
        //? test proccess
        // console.log("3. failed get data", error);
        //? end test proccess

        dispatch({
          type: DELETE_CONTACT,
          payload: {
            loading: false,
            data: false,
            errorMessage: error.message
          }
        });
      });
  };
};

export const detailContact = (data: any) => {
  return (dispatch: any) => {
    dispatch({
      type: DETAIL_CONTACT,
      payload: {
        data: data
      }
    });
  };
};

export const updateContact = (data: any) => {
  //? test proccess
  // console.log("2. enter action");
  //? end test proccess

  return (dispatch: any) => {
    // loading
    dispatch({
      type: UPDATE_CONTACT,
      payload: {
        loading: true,
        data: false,
        errorMessage: false
      }
    });

    //? Success Get API
    axios({
      method: "PUT",
      url: `http://localhost:3000/contact/` + data.id,
      timeout: 12000,
      data: data
    })
      .then((response) => {
        //? test proccess
        // console.log("3. success get data", response.data);
        //? end test proccess

        dispatch({
          type: UPDATE_CONTACT,
          payload: {
            loading: false,
            data: response.data,
            errorMessage: false
          }
        });
      })
      //? failed Get API
      .catch((error) => {
        //? test proccess
        // console.log("3. failed get data", error);
        //? end test proccess

        dispatch({
          type: UPDATE_CONTACT,
          payload: {
            loading: false,
            data: false,
            errorMessage: error.message
          }
        });
      });
  };
};
