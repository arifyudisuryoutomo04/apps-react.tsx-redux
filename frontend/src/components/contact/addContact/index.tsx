import {
  Button,
  FormControl,
  FormLabel,
  Heading,
  Input
} from "@chakra-ui/react";
import {
  addContact,
  getListContact,
  updateContact
} from "../../../actions/contact";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

function AddContact() {
  const [name, setName] = useState("");
  const [M_Number, setM_Number] = useState("");
  const [id, setId] = useState("");

  const dispatch = useDispatch();

  //? auto reload/GET
  const { addContactResult, detailContactResult, updateContactResult } =
    useSelector((state: any) => state.contactReducers);

  useEffect(() => {
    if (addContactResult) {
      //? test proccess
      // console.log("5. enter components did updates");
      //? end test proccess

      dispatch(getListContact() as any);
      setName("");
      setM_Number("");
    }
  }, [addContactResult, dispatch]);

  useEffect(() => {
    if (detailContactResult) {
      setName(detailContactResult.name);
      setM_Number(detailContactResult.mobile_number);
      setId(detailContactResult.id);
    }
  }, [detailContactResult, dispatch]);

  useEffect(() => {
    if (updateContactResult) {
      //? test proccess
      // console.log("5. enter components did updates");
      //? end test proccess

      dispatch(getListContact() as any);
      setName("");
      setM_Number("");
      setId("");
    }
  }, [updateContactResult, dispatch]);
  //? end auto reload/GET

  const handleSubmit = (event: any) => {
    event.preventDefault(); //? remove reload function
    if (id) {
      //? update contact
      dispatch(
        updateContact({ id: id, name: name, mobile_number: M_Number }) as any
      );
    } else {
      //? add contact
      dispatch(addContact({ name: name, mobile_number: M_Number }) as any);
    }
    //? test proccess
    // console.log("1. enter handle submit");
    //? end test proccess
  };

  return (
    <>
      {/* use Heading ternary operator */}
      <Heading>{id ? "Edit Contact" : "Add Contact"}</Heading>
      <form onSubmit={(event) => handleSubmit(event)}>
        <FormControl>
          <FormLabel>Name</FormLabel>
          <Input
            type="text"
            name="name"
            placeholder="Name . . ."
            value={name}
            onChange={(event) => setName(event.target.value)}
          />
          <FormLabel>Mobile Number</FormLabel>
          <Input
            type="text"
            name="mobile_number"
            placeholder="Mobile Number . . ."
            value={M_Number}
            onChange={(event) => setM_Number(event.target.value)}
          />
          <Button type="submit">Submit</Button>
        </FormControl>
      </form>
    </>
  );
}

export default AddContact;
