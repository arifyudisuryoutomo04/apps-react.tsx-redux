import {
  Button,
  Flex,
  IconButton,
  Popover,
  PopoverArrow,
  PopoverBody,
  PopoverContent,
  PopoverTrigger,
  Stack,
  Table,
  TableCaption,
  TableContainer,
  Tbody,
  Td,
  Text,
  Tfoot,
  Th,
  Thead,
  Tr
} from "@chakra-ui/react";
import {
  getListContact,
  deleteContact,
  detailContact
} from "../../../actions/contact";
import React, { useEffect } from "react";
import { BsThreeDotsVertical } from "react-icons/bs";
import { useDispatch, useSelector } from "react-redux";
import { DeleteIcon, EditIcon } from "@chakra-ui/icons";

function ListContact() {
  const {
    getListContactResult,
    getListContactLoading,
    getListContactError,
    deleteContactResult
  } = useSelector((state: any) => state.contactReducers);

  const dispatch = useDispatch();

  useEffect(() => {
    //? test proccess
    // console.log("1. use effect components did mount");
    //? end test proccess

    //? Call Action ListContact
    dispatch(getListContact() as any);
  }, [dispatch]);

  //? auto reload/GET

  useEffect(() => {
    if (deleteContactResult) {
      //? test proccess
      // console.log("5. enter components did updates");
      //? end test proccess

      dispatch(getListContact() as any);
    }
  }, [deleteContactResult, dispatch]);
  //? end auto reload/GET

  return (
    <>
      <TableContainer pt={6}>
        <Table variant="striped" colorScheme="teal">
          <TableCaption>List Contact</TableCaption>
          <Thead>
            <Tr>
              <Th>No.</Th>
              <Th>Name</Th>
              <Th>Mobile Number</Th>
              <Th>actions</Th>
            </Tr>
          </Thead>
          <Tbody>
            {getListContactResult ? (
              getListContactResult.map((contact: any, index: any) => {
                return (
                  <Tr key={contact.id}>
                    <Td>{index + 1}</Td>
                    <Td>{contact.name}</Td>
                    <Td>{contact.mobile_number}</Td>
                    <Td>
                      <Flex justifyContent="center" mt={4}>
                        <Popover placement="bottom" isLazy>
                          <PopoverTrigger>
                            <IconButton
                              aria-label="More server options"
                              icon={<BsThreeDotsVertical />}
                              variant="solid"
                              w="fit-content"
                            />
                          </PopoverTrigger>
                          <PopoverContent
                            w="fit-content"
                            _focus={{ boxShadow: "none" }}
                          >
                            <PopoverArrow />
                            <PopoverBody>
                              <Stack>
                                <Button
                                  w="194px"
                                  variant="ghost"
                                  rightIcon={<DeleteIcon />}
                                  justifyContent="space-between"
                                  fontWeight="normal"
                                  fontSize="sm"
                                  colorScheme="red"
                                  onClick={() =>
                                    dispatch(deleteContact(contact.id) as any)
                                  }
                                >
                                  Hapus
                                </Button>
                                <Button
                                  w="194px"
                                  variant="ghost"
                                  rightIcon={<EditIcon />}
                                  justifyContent="space-between"
                                  fontWeight="normal"
                                  colorScheme="green"
                                  fontSize="sm"
                                  onClick={() =>
                                    dispatch(detailContact(contact) as any)
                                  }
                                >
                                  Edit
                                </Button>
                              </Stack>
                            </PopoverBody>
                          </PopoverContent>
                        </Popover>
                      </Flex>
                    </Td>
                  </Tr>
                );
              })
            ) : getListContactLoading ? (
              <Text>. . . Loading</Text>
            ) : (
              <Text>
                {getListContactError ? getListContactError : "blank data"}
              </Text>
            )}
          </Tbody>
          <Tfoot>
            <Tr>
              <Th>No.</Th>
              <Th>Name</Th>
              <Th>Mobile Number</Th>
              <Th>actions</Th>
            </Tr>
          </Tfoot>
        </Table>
      </TableContainer>
    </>
  );
}

export default ListContact;
