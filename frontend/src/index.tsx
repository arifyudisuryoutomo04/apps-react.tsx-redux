import { ChakraProvider, theme } from "@chakra-ui/react";
import * as ReactDOM from "react-dom/client";
import { App } from "./App";
import Footer from "./components/footer";
import Navbar from "./components/navbar";
import * as React from "react";
import { createStore, compose, applyMiddleware } from "@reduxjs/toolkit";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import reducers from "./reducers";

const store = createStore(
  reducers,
  compose(
    applyMiddleware(thunk),
    (window as any).__REDUX_DEVTOOLS_EXTENSION__ &&
      (window as any).__REDUX_DEVTOOLS_EXTENSION__()
  )
);

const container = document.getElementById("root");
if (!container) throw new Error("Failed to find the root element");
const root = ReactDOM.createRoot(container);

root.render(
  <React.StrictMode>
    <Provider store={store}>
      <ChakraProvider theme={theme}>
        <Navbar />
        <App />
        <Footer />
      </ChakraProvider>
    </Provider>
  </React.StrictMode>
);
